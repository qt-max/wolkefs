WolkeFS design
==============

What is WolkeFS
---------------

WolkeFS is a tool to build your own file storage with cloud-like features:
selective synchronization, offline mode, transparency. Wolke is German for
cloud.

Goals of WolkeFS
----------------

The main goal of WolkeFS is to provide user experience of a file storage cloud.
This includes the following features:

- Self-hosted.

- Transparency.

  - Applications should work with cloud files as if they were local.

- Offline mode.

  - The user can choose some files to be accessible offline.
  - Changes are synchronized when going back online.
  - The whole directory tree and file lists are visible offline.

- Synchronization.

  - The files stored locally are synchronized with the server.
  - The server stores all the files.
  - External changes on the server side are propagated to the clients.

- Security.

  - All communications to the server are encrypted.
  - Server- and client-side disk encryption is out of scope.
  - Client-side file encryption is low priority.

- Interoperation.

  - Compatibility with other remote FS and synchronization software.
  - The server detects external changes to the files and directories.

Main challenges
---------------

- Underlying remote filesystem protocol.

  - Basic file operations, data transfer, notifications.

    - Custom binary protocol based on Cap'n Proto format.

  - Encryption.

    - SSH.

- Detecting external changes on the server and propagation to the clients.

  - Use inotify, rather than assume that only clients can do changes.
  - Avoid storing additional metadata in xattr/database on the server.

- Detecting server-side changes on clients.

  - Server uses inotify and communicates changes via the custom protocol.

- VFS abstraction over the remote FS and local cache.

  - Custom FUSE filesystem.
  - Possibly integration with file managers (plugins).

    - Local (cached) files to be opened by applications directly.

- Partial file synchronization.

  - Avoid redownloading the whole file.
  - Files are divided into blocks, the custom protocol operates with them.

- UI to set a file as available/unavailable offline.

  - Integration with file managers (plugins).
  - Dedicated application that will also allow to edit policies, etc.

- Solve file conflicts.

  - A client goes online and wants to upload a file, but it has been already
    updated by another client.

Components
----------

Sync server
***********

- Stores all files.
- Accepts client connections and maintains event-based communication.
- Watches for file changes with inotify and notifies the clients.
- Provides the directory structure and file blocks to the clients.
- Serializes the clients trying to modify the same file.

  - No conflicts are created on the server side.
  - Allows clients to change the file only if sync timestamps match.
  - Blocks other clients from changing the file at the same time.
  - After the change, other clients' changes will be rejected, they will get a
    notification and create a conflict locally.

Sync client
***********

- Stores files selected for sync in a local cache directory.
- Stores empty placeholders for unavailable (remote) files.
- Uses xattr to distinguish placeholders.
- Maintains a database with sync timestamps of files.
- Maintains a connection to the server.
- Watches for file changes with inotify and syncs changes to the server.
- Watches for changes to the placeholder flag and downloads/removes files.
- Performs single-time whole-tree sync on startup/connection.
- Detects conflicts, preferring the remote version and renaming the local one.

  - The simplest, but not the safest option.

    - Some editors might overwrite the newer remote version on save.

  - A good alternative:

    - Keep the local version without updating its sync timestamp.

      - It will prevent it from being uploaded.

    - Download the remote version under a different name.

      - Set a flag in xattr to avoid guessing based on names.

    - Solving conflicts:

      - Removing the remote version will choose the local one.

        - Sync timestamp will be set to remote ctime to reenable upload.

      - Removing the local version will choose the remote one.

        - The file will be renamed back to its name.
        - Upload for the local file is blocked - removal won't hurt the server.

- Supports sync policies (defined in xattr of directories):

  - Don't download new remote files by default.
  - Download new remote files by default.
  - Use the parent setting for downloading new remote files by default.
  - The same for subdirectories.
  - Quotas, i.e. don't download files/directories over a given size.
  - Policy propagation for new and synced subdirectories:

    - Use the parent policy and policy propagation rules.
    - Assign a new policy and/or policy propagation rules.

- Deals with files locked locally.

  - If a file was changed remotely, wait until the local lock is released.

Transport layer
***************

Sync server and client will communicate via stdin/stdout with the transport
layer. It allows to choose different transport layers, given that they provide
reliable delivery and enough security. SSH is considered as the "official"
transport layer, providing encrypted communication.

FUSE adaptor
************

- Provides a whole-tree view in the VFS.

  - Local/remote flag is exposed via xattr.
  - Remote file sizes are stored in xattr of placeholders in the cache.
  - Removing a local file makes it remote.

    - Some editors might remove a file and rename a temporary one on save.

      - It will lead to redownloading the file just to be rewritten.
      - May be worked around by avoiding download if the file is truncated.
      - Proper editors will rename atomically, without removing the file.

    - The placeholder flag is set before truncating the file in the cache.

      - The file in the cache must be locked to avoid concurrent downloading.

  - Removing a remote file deletes it from the server.

- Supports policies for opening remote files:

  - Download the file automatically.
  - Ask the user: download or abort.

    - On abort, return an error from open().

  - Open remotely by transferring blocks over the network.

    - Not safe against simultaneous edits.
    - Not safe against network failures.
    - Slow, as file access happens synchronously over the network.

  - Defined per file in xattr of placeholders and files in the cache.
  - Directories define propagation rules for new and synced files.
  - Directories define propagation rules for new and synced directories.
  - Quotas, i.e. decide on the action depending on file size.

- Communicate to the UI via D-Bus when user interaction is needed.

  - If UI is unavailable, fall back to a safe action (such as abort).

- Drawback: FUSE implies an extra copy on every access to the local file.

User interface
**************

- Shows messages from FUSE adaptor.
- Communicates user decisions to FUSE adaptor.
- Allows to download files without opening them.
- Allows to edit all kinds of policies.

Sync and conflict detection algorithm
-------------------------------------

A conflict is a situation when client A wants to upload a new version of file F
to the server, but the server already has a version of file F newer than client
A last saw. Example flow:

- Client A goes offline.
- User changes file F on client A (change 1).
- Client B is online.
- User changes file F on client B (change 2).
- Client B propagates change 2 to the server.
- Client A goes online.
- Client A wants to propagate change 1 to the server, but can't.

The sync client maintains a database that stores the list of files, their inode
numbers, permissions and stimes. Stime stands for sync timestamp, and it's equal
to mtime (modification time) of the file at the moment it's synced.

  Although ctime (change time) looks more appropriate (it changes on every
  content or metadata change and can't be manipulated by the user), it can't
  really be used, because it changes on many unrelated metadata changes (such as
  xattrs used by wolkefs), and it won't be possible to have the same ctimes on
  the server and on the client.

  On the other hand, mtime changes on content changes, and also when the user
  sets it manually, so it fits well for the purpose of wolkefs.

The algorithm:

- If `remote_mtime == stime == local_mtime`, no action is required.
- If `remote_mtime == stime != local_mtime`, upload the file.
- If `remote_mtime != stime == local_mtime`, download the file.
- If `remote_mtime != stime != local_mtime`, report a conflict.

Timestamp granularity may be different on different machines, so precision may
be lost when copying the remote mtime to the local one, leading to wrong
comparison results. To avoid such kind of bugs, two stimes may be stored — with
client and server precision — to be used in corresponding comparisons.

Normally, when the sync client is online and running, changes in the cache are
monitored by inotify, the sync server notifies the client about remote changes.
When the sync client goes online (after start or restoring the connection), a
full tree comparison happens: the whole tree is traversed to find changes made
locally and remotely and act accordingly to the algorithm above.

Deleting files
**************

Stimes are stored in a separate database, rather than in xattr of the
corresponding files, because when a file gets deleted, it would be impossible to
recover its xattr and apply the algorithm above.

Mtime is simpler — even though we can't recover it, we consider it to be not
equal to stime.

Renaming files
**************

While it's easy to detect renames with inotify, some renames may happen while
sync client is not running. To detect them, inode numbers are used, i.e. on
startup the client finds out that some file was removed, another file was
created, and they have the same inode number, then it's a rename.
